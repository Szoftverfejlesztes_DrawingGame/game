﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class WebcamTest : MonoBehaviour
{
    public int savenum=0;
    public string deviceName;
    WebCamTexture wct;
    string datapath;
    Color _colorHolder = Color.white;
    Texture2D _textureHolder;

    // Use this for initialization
    void Start()
    {
        if (!File.Exists(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/save.txt"))
        {
            File.CreateText(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/save.txt");
        }
        bool exitBool = true;
        while (exitBool) {
            if (File.Exists(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/photo" + savenum + ".png"))
            {
                savenum++;
            }
            else
            {
                exitBool = false;
            }
        }
        datapath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/photo"+savenum+".png";
        string newPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), "myunitygame");
        System.IO.Directory.CreateDirectory(newPath);
        newPath = System.IO.Path.Combine(newPath, "s");
        System.IO.Directory.CreateDirectory(newPath);

        WebCamDevice[] devices = WebCamTexture.devices;
        deviceName = devices[0].name;
        wct = new WebCamTexture(deviceName, 400, 300, 12);
        this.GetComponent<RawImage>().material.mainTexture = wct;
        wct.Play();

        byte[] data = File.ReadAllBytes(datapath);
        Texture2D texture = new Texture2D(10, 10, TextureFormat.ARGB32, false);
        texture.LoadImage(data);
        _textureHolder = texture;
        texture.name = Path.GetFileNameWithoutExtension(datapath);

        Slice(texture);
    }

    // For photo varibles

    public Texture2D heightmap;
    public Vector3 size = new Vector3(100, 10, 100);


    public void Slice(Texture2D texture)
    {
        Color[] c = texture.GetPixels(150, 70, 50, 100);
        Texture2D m2Texture = new Texture2D(50, 100);
        m2Texture.SetPixels(c);
        m2Texture.Apply();

        Sprite mySprite = Sprite.Create(m2Texture, new Rect(0.0f, 0.0f, m2Texture.width, m2Texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        GameObject.Find("Player_Sprite").GetComponent<SpriteRenderer>().sprite = mySprite;
    }

    public void TakeSnapshot()
    {
        Texture2D snap = new Texture2D(wct.width, wct.height);
        snap.SetPixels(wct.GetPixels());
        snap.Apply();

        System.IO.File.WriteAllBytes(datapath, snap.EncodeToPNG());

        byte[] data = File.ReadAllBytes(datapath);
        Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
        texture.LoadImage(data);
        texture.name = Path.GetFileNameWithoutExtension(datapath);

        Slice(texture);

        
        List<Level> newLevelList = new List<Level>();
        newLevelList.Add(new Level());
        newLevelList.Add(new Level());
        newLevelList.Add(new Level());
        var tempchar = new Character(savenum, newLevelList);
        GameObject.Find("GameManager").GetComponent<GameManagerScript>().characters.Add(tempchar);
        GameObject.Find("GameManager").GetComponent<GameManagerScript>().current_char = tempchar;
        GameObject.Find("GameManager").GetComponent<GameManagerScript>().SaveCharacter();
        //GameObject.Find("GameManager").GetComponent<GameManagerScript>().activeSave = savenum;

        GameObject.Find("LoadGameText").GetComponent<LoadGameScript>().LoadChar(GameObject.Find("GameManager").GetComponent<GameManagerScript>().current_char);
    }

    public void Slider_ThreshHold(float num)
    {
        //GameObject.Find("Image").GetComponent<Shader>(). = Shader.Find("No_BackGround");
        //GameObject.Find("Image").GetComponent<Shader>().SetFloat("Treshhold", num);
    }
}