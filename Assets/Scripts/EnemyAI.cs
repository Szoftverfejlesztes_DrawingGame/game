﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public Transform target;
    public float speed = 2f;

    bool stop = false;

    // Use this for initialization
    void Start () {
        target = GameObject.Find("Player").transform;
    }
	
	// Update is called once per frame
	void Update () {
        //rotate to look at the player
        transform.LookAt(target.position);
        transform.Rotate(new Vector3(0, -90, 0), Space.Self);//correcting the original rotation


        //move towards the player
        if (Vector3.Distance(transform.position, target.position) > 1f)
        {//move if distance from target is greater than 1
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
    }

    IEnumerator SlowKill(GameObject go)
    {
        yield return new WaitForSeconds(3);
        speed = 2f;
        Destroy(go);
        target = GameObject.Find("Player").transform;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "New Game Object") {
            speed = 0f;
            //target = coll.gameObject.transform;
            StartCoroutine(SlowKill(coll.gameObject));
        }
        if (coll.gameObject.name == "Player")
        {
            GameObject.Find("LevelPanel").GetComponent<LevelSelectScript>().Death();
            Debug.Log("You died");
        }
    }
}
