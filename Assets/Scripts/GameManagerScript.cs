﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

public class Level
{
    public Level()
    {

    }
    public Level(bool l, bool c, int s, float t)
    {
        locked = l;
        completed = c;
        stars = s;
        time = t;
    }
    public bool locked = true;
    public bool completed = false;
    public int stars = 0;
    public float time = 0;
    public Dictionary<DateTime, int> timeToStars = new Dictionary<DateTime, int>();
}

public class Character
{
    public Character(int s, List<Level> p)
    {
        savenum = s;
        progression = p;
    }

    public Character()
    {
        progression.Add(new Level());
    }

    public int savenum;
    public List<Level> progression = new List<Level>();
    public string getData()
    {
        string levelprogress = "";
        foreach(Level l in progression)
        {
            levelprogress += "," + l.locked.ToString() + "," + l.completed.ToString() + "," + l.stars.ToString() + "," + l.time.ToString();
        }
        return savenum.ToString() + levelprogress;
    }
}

public class GameManagerScript : MonoBehaviour {

    public List<Character> characters = new List<Character>();

    public GameObject cam;
    public GameObject player;
    public enum camtarget {player, mainmenu, charcreation, levelselect};
    public camtarget target;

    public Level current_level;
    public Character current_char;

    public enum playerposition { charcreation, levelselect, level, done};
    public playerposition playerpos;

    //Camera variables
    float maxScreenPoint = 0.8f;
    Vector3 velocity;
    float dampTime = 1f;

    // Use this for initialization
    void Start () {
        target = camtarget.mainmenu;
        playerpos = playerposition.levelselect;
        LoadSavedCharacters();
    }
	
	// Update is called once per frame
	void Update () {
        if (playerpos == playerposition.charcreation)
        {
            player.transform.position = new Vector2(-21.78f, 24.94f);
            playerpos = playerposition.done;
        }else if (playerpos == playerposition.levelselect)
        {
            player.transform.position = new Vector2(28.45f, 32.2f);
            playerpos = playerposition.done;
        
        }else if (playerpos == playerposition.level)
        {
            player.transform.position = new Vector2(0, 0);
            playerpos = playerposition.done;
        }

        if (target == camtarget.charcreation)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, new Vector2(-23, 27), 2.0f * Time.deltaTime);
        } else if (target == camtarget.levelselect)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, new Vector2(23, 27), 2.0f * Time.deltaTime);
        }
        else if (target == camtarget.player)
        {
            
            /*
            Vector3 mousePos = Input.mousePosition * maxScreenPoint + new Vector3(Screen.width, Screen.height, 0f) * ((1f - maxScreenPoint) * 0.5f);
            Vector3 position = (player.transform.position + cam.GetComponentInChildren<Camera>().ScreenToWorldPoint(mousePos)) / 2f;
            Vector3 destination = new Vector3(position.x, position.y, -10);
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            */
            cam.transform.position = Vector3.Lerp(cam.transform.position, player.transform.position, 2.0f * Time.deltaTime);
        }
    }

    public void LoadSavedCharacters()
    {
        string path = Environment.ExpandEnvironmentVariables(@"%APPDATA%\myunitygame\saves");
        if (File.Exists(path + "/save.txt"))
        {
            using (StreamReader sr = File.OpenText(path + "/save.txt"))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    Debug.Log(s);
                    string[] tokens = s.Split(',');
                    List<Level> loadedLevels = new List<Level>();
                    for (int i = 1; i<tokens.Length; i+=4)
                    {
                        Debug.Log(Convert.ToBoolean(tokens[i]));
                        Level l = new Level(Convert.ToBoolean(tokens[i]), Convert.ToBoolean(tokens[i +1]), Int32.Parse(tokens[i +2]), float.Parse(tokens[i +3], CultureInfo.InvariantCulture.NumberFormat));
                        loadedLevels.Add(l);
                    }
                    Character temp = new Character(Int32.Parse(tokens[0]), loadedLevels);
                    characters.Add(temp);
                }
            }
            
        }
    }

    public void SaveCharacter()
    {
        string path = Environment.ExpandEnvironmentVariables(@"%APPDATA%\myunitygame\saves");
        using (StreamWriter sw = new System.IO.StreamWriter(path + "/save.txt"))
        {
            foreach (Character c in characters)
            {
                sw.WriteLine(c.getData());
            }
        }
    }
}
