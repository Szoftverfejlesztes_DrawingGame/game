﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGameScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseEnter()
    {
        this.gameObject.GetComponent<TextMesh>().color = Color.gray;
    }

    void OnMouseExit()
    {
        this.gameObject.GetComponent<TextMesh>().color = Color.black;
    }

    void OnMouseDown()
    {
        Debug.Log("Exiting");
        Application.Quit();
    }
}
