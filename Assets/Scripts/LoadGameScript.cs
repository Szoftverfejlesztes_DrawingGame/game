﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LoadGameScript : MonoBehaviour {

    public GameObject characterPanel;
    public GameObject characterButton;
    public Material nobgmat;
    public GameObject levelButton;

    GameManagerScript gm;

	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseEnter()
    {
        this.gameObject.GetComponent<TextMesh>().color = Color.gray;
    }

    void OnMouseExit()
    {
        this.gameObject.GetComponent<TextMesh>().color = Color.black;
    }

    void OnMouseDown()
    {
        //GameObject.Find("GameManager").GetComponent<GameManagerScript>().target = GameManagerScript.camtarget.levelselect;
        //GameObject.Find("GameManager").GetComponent<GameManagerScript>().playerpos = GameManagerScript.playerposition.levelselect;
        characterPanel.SetActive(true);
        foreach(Transform child in characterPanel.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Character c in gm.characters)
        {
            var temp = Instantiate(characterButton, new Vector3(0, 0, 0), Quaternion.identity);
            temp.GetComponent<Button>().onClick.AddListener(() => LoadChar(c));

            string datapath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/photo" + c.savenum + ".png";
            byte[] data = File.ReadAllBytes(datapath);
            Texture2D texture = new Texture2D(10, 10, TextureFormat.ARGB32, false);
            texture.LoadImage(data);
            texture.name = Path.GetFileNameWithoutExtension(datapath);

            Color[] col = texture.GetPixels(150, 70, 50, 100);
            Texture2D m2Texture = new Texture2D(50, 100);
            m2Texture.SetPixels(col);
            m2Texture.Apply();

            Sprite mySprite = Sprite.Create(m2Texture, new Rect(0.0f, 0.0f, m2Texture.width, m2Texture.height), new Vector2(0.5f, 0.5f), 100.0f);

            var tempobject = new GameObject();
            var tempimage = tempobject.AddComponent<Image>();
            tempimage.sprite = mySprite;
            tempobject.GetComponent<RectTransform>().SetParent(temp.transform);
            tempimage.material = nobgmat;
            temp.transform.parent = characterPanel.transform;
            temp.transform.localScale = new Vector3(1, 1, 1);

            tempobject.transform.localScale = new Vector3(0.11f, 0.22f, 1);
        }
    }

    public void LoadChar(Character c)
    {
        gm.current_char = c;
        string datapath= System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "/myunitygame/saves/photo" + c.savenum + ".png";
        byte[] data = File.ReadAllBytes(datapath);
        Texture2D texture = new Texture2D(10, 10, TextureFormat.ARGB32, false);
        texture.LoadImage(data);
        texture.name = Path.GetFileNameWithoutExtension(datapath);

        GameObject.Find("WebcamImage").GetComponent<WebcamTest>().Slice(texture);

        foreach (Transform child in GameObject.Find("LevelPanel").transform)
        {
            Destroy(child.gameObject);
        }

        var temp = Instantiate(levelButton);
        temp.transform.parent = GameObject.Find("LevelPanel").transform;
        temp.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("LevelPanel").GetComponent<LevelSelectScript>().Level1());
        temp.transform.GetChild(0).GetComponent<TextMesh>().text = "Level 01";
        temp.transform.GetChild(1).GetComponent<TextMesh>().text = "Time: \n" + gm.current_char.progression[0].time;
        if(gm.current_char.progression[0].stars > 0)
        {
            temp.transform.GetChild(2).GetComponent<TextMesh>().text = "★";
        }
        else
        {
            temp.transform.GetChild(2).GetComponent<TextMesh>().text = "N/A";
        }

        var temp1 = Instantiate(levelButton);
        temp1.transform.parent = GameObject.Find("LevelPanel").transform;
        temp1.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("LevelPanel").GetComponent<LevelSelectScript>().Level2());
        temp1.transform.GetChild(0).GetComponent<TextMesh>().text = "Level 02";
        temp1.transform.GetChild(1).GetComponent<TextMesh>().text = "Time: \n" + gm.current_char.progression[1].time;
        if (gm.current_char.progression[1].stars > 0)
        {
            temp1.transform.GetChild(2).GetComponent<TextMesh>().text = "★";
        }
        else
        {
            temp1.transform.GetChild(2).GetComponent<TextMesh>().text = "N/A";
        }

        var temp2 = Instantiate(levelButton);
        temp2.transform.parent = GameObject.Find("LevelPanel").transform;
        temp2.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("LevelPanel").GetComponent<LevelSelectScript>().Level3());
        temp2.transform.GetChild(0).GetComponent<TextMesh>().text = "Level 03";
        temp2.transform.GetChild(1).GetComponent<TextMesh>().text = "Time: \n" + gm.current_char.progression[2].time;
        if (gm.current_char.progression[2].stars > 0)
        {
            temp2.transform.GetChild(2).GetComponent<TextMesh>().text = "★";
        }
        else
        {
            temp2.transform.GetChild(2).GetComponent<TextMesh>().text = "N/A";
        }

        GameObject.Find("GameManager").GetComponent<GameManagerScript>().target = GameManagerScript.camtarget.levelselect;
        GameObject.Find("GameManager").GetComponent<GameManagerScript>().playerpos = GameManagerScript.playerposition.levelselect;
    }
}
