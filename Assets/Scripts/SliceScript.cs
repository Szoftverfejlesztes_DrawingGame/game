﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceScript : MonoBehaviour {
    public Texture2D mTexture;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Slice() {
        mTexture = this.gameObject.GetComponent<SpriteRenderer>().sprite.texture;
    
        
        Sprite mySprite = Sprite.Create(mTexture, new Rect(0.0f, 0.0f, mTexture.width/2, mTexture.height/2), new Vector2(0.5f, 0.5f), 100.0f);
        gameObject.GetComponent<SpriteRenderer>().sprite = mySprite;
    }
}
