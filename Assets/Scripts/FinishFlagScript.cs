﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishFlagScript : MonoBehaviour {

    GameObject levelselect;

	// Use this for initialization
	void Start () {
        levelselect = GameObject.Find("LevelPanel");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Player")
        {
            Debug.Log("You win");
            levelselect.GetComponent<LevelSelectScript>().Completed();
        }
    }
}
