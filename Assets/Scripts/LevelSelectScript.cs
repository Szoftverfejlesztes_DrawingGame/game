﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectScript : MonoBehaviour {

    GameManagerScript gm;
    float time=0;
    public GameObject uicanvas;
    public GameObject timerui;
    Transform playablearea;
    public GameObject[] maps;
    public GameObject eraser;
    GameObject active_eraser;

    int curr_lvl = 0;

	// Use this for initialization
	void Start () {
        gm=GameObject.Find("GameManager").GetComponent<GameManagerScript>();
        playablearea = GameObject.Find("PlayableArea").transform;

    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        timerui.GetComponent<Text>().text = "Time: " + time;
    }

    public void Death()
    {
        Destroy(active_eraser);
        playablearea.GetComponent<Draw>().drawBool = false;
        GameObject.Find("DeathText").GetComponent<Animator>().Play("death");
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        uicanvas.SetActive(false);
        if (gm.current_level == gm.current_char.progression[0])
        {
            Level1();
        } else
        if (gm.current_level == gm.current_char.progression[1])
        {
            Level2();
        } else
        if (gm.current_level == gm.current_char.progression[2])
        {
            Level3();
        }
    }

    public void Completed()
    {
        Destroy(active_eraser);
        playablearea.GetComponent<Draw>().drawBool = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        float ctime = time;
        foreach(Level l in gm.current_char.progression)
        {
            if(l == gm.current_level)
            {
                if (l.time == 0 || l.time > ctime)
                {
                    l.time = ctime;
                }
                l.completed = true;
                l.stars = 1;
            }
        }
        gm.SaveCharacter();
        uicanvas.SetActive(false);
        GameObject.Find("LoadGameText").GetComponent<LoadGameScript>().LoadChar(gm.current_char);
        gm.playerpos = GameManagerScript.playerposition.levelselect;
        gm.target = GameManagerScript.camtarget.levelselect;
    }

    public void Reload()
    {
        Destroy(active_eraser);
        playablearea.GetComponent<Draw>().drawBool = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        uicanvas.SetActive(false);
        if (curr_lvl == 1)
        {
            Level1();
        } else
            if(curr_lvl == 2){
            Level2();
        } else
            if (curr_lvl == 3)
        {
            Level3();
        }
    }

    public void BackToLevelSelect()
    {
        Destroy(active_eraser);
        playablearea.GetComponent<Draw>().drawBool = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        uicanvas.SetActive(false);
        gm.playerpos = GameManagerScript.playerposition.levelselect;
        gm.target = GameManagerScript.camtarget.levelselect;
    }

    IEnumerator Example()
    {
        GameObject.Find("CountDown3").GetComponent<Animator>().Play("countdown");
        yield return new WaitForSeconds(1.5f);
        GameObject.Find("CountDown2").GetComponent<Animator>().Play("countdown");
        yield return new WaitForSeconds(1.5f);
        GameObject.Find("CountDown1").GetComponent<Animator>().Play("countdown");
        yield return new WaitForSeconds(1.5f);
        GameObject.Find("CountDown0").GetComponent<Animator>().Play("countdown");
        uicanvas.SetActive(true);
        GameObject.Find("Player").GetComponent<PlayerScript>().move = true;
        GameObject.Find("PlayableArea").GetComponent<Draw>().drawBool = true;
        time = 0;
    }

    public void Level1()
    {
        curr_lvl = 1;
        gm.current_level = gm.current_char.progression[0];
        GameObject.Find("Player").GetComponent<PlayerScript>().move = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        var map = Instantiate(maps[0]);
        map.transform.parent = playablearea;
        map.transform.localScale = new Vector3(1, 1, 1);
        gm.playerpos = GameManagerScript.playerposition.level;
        gm.target = GameManagerScript.camtarget.player;
        //coutdown
        StartCoroutine(Example());
        
    }

    public void Level2()
    {
        curr_lvl = 2;
        gm.current_level = gm.current_char.progression[1];
        GameObject.Find("Player").GetComponent<PlayerScript>().move = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        var map = Instantiate(maps[1]);
        map.transform.parent = playablearea;
        map.transform.localScale = new Vector3(1, 1, 1);
        gm.playerpos = GameManagerScript.playerposition.level;
        gm.target = GameManagerScript.camtarget.player;
        //coutdown
        StartCoroutine(Example());

    }

    public void Level3()
    {
        curr_lvl = 3;
        gm.current_level = gm.current_char.progression[2];
        GameObject.Find("Player").GetComponent<PlayerScript>().move = false;
        foreach (Transform child in playablearea)
        {
            Destroy(child.gameObject);
        }
        var map = Instantiate(maps[2]);
        map.transform.parent = playablearea;
        map.transform.localScale = new Vector3(1, 1, 1);
        gm.playerpos = GameManagerScript.playerposition.level;
        gm.target = GameManagerScript.camtarget.player;
        //coutdown
        StartCoroutine(Example());
        active_eraser = Instantiate(eraser, new Vector3(30,0,0), Quaternion.identity);

    }
}
